 /* * * * * * * * * * * * * * * * * * * *
 | Script Author: Orion                 |
 | Script Version: 1.0                  |
 | Release Date: 20.10.2011             |
 | Credits to:                          |
 |           	-weedarr.wikidot.com    |
 |              -Spectre                |
 |              -BechzyBoi              |
 * * * * * * * * * * * * * * * * * * * */



#define FILTERSCRIPT

#include <a_samp>
#include <nex-ac>

//================================[SETTINGS]====================================

#define 	AdminOnly 	false // Change this to false if you want that everybody can use it.
#define 	DIALOGID 	2488 // Change this to an id you don't use in your script.

//=============================[TELEPORT NAMES]=================================

new OrTeleports[][] =
{
	{"24/7 1"},
	{"24/7 2"},
	{"24/7 3"},
	{"24/7 4"},
	{"24/7 5"},
	{"24/7 6"},
	{"Airport Ticket Desk"},
	{"Airport Baggage Reclaim"},
	{"Shamal"},
	{"Andromada"},
	{"Ammunation 1"},
	{"Ammunation 2"},
	{"Ammunation 3"},
	{"Ammunation 4"},
	{"Ammunation 5"},
	{"Ammunation Booths"},
	{"Ammunation Range"},
	{"Blastin Fools Hallway"},
	{"Budget Inn Motel Room"},
	{"Jefferson Motel"},
	{"Off Track Betting Shop"},
	{"Sex Shop"},
	{"Meat Factory"},
	{"Zero's RC Shop"},
	{"Dillmore Gas Station"},
	{"Caligula's Basement"},
	{"FDC Janitors Room"},
	{"Woozie's Office"},
	{"Binco"},
	{"Didier Sachs"},
	{"Prolaps"},
	{"Suburban"},
	{"Victim"},
	{"ZIP"},
	{"Alhambra"},
	{"Ten Bottles"},
 	{"Lil' Probe Inn"},
 	{"Jay's Dinner"},
 	{"Gant Bridge Dinner"},
	{"Secret Valley Dinner"},
	{"World of Coq"},
	{"Welcome Pump"},
	{"Burger Shot"},
	{"Cluckin' Bell"},
	{"Well Stacked Pizza"},
	{"Jimmy's Sticky Ring"},
	{"Denise Room"},
	{"Katie Room"},
	{"Helena Room"},
	{"Michelle Room"},
	{"Barbara Room"},
	{"Mille Room"},
	{"Sherman Dam"},
	{"Planning Dept."},
	{"Area 51"},
	{"LS Gym"},
	{"SF Gym"},
	{"LV Gym"},
	{"B Dup's House"},
	{"B Dup's Crack Pad"},
	{"CJ's House"},
	{"Madd Dogg's Mansion"},
	{"OG Loc's House"},
	{"Ryder's House"},
	{"Sweet's House"},
	{"Crack Factory"},
	{"Big Spread Ranch"},
	{"Fanny Batters"},
	{"Strip Club"},
	{"Strip Club Private Room"},
	{"Unnamed Brothel"},
	{"Tiger Skin Brothel"},
	{"Pleasure Domes"},
	{"Liberty City Outside"},
	{"Liverty City Inside"},
	{"Gang House"},
	{"Colonel Furhberger's House"},
	{"Crack Den"},
	{"Warehouse 1"},
	{"Warehouse 2"},
	{"Sweet's Garage"},
	{"Lil' Probe Inn Toilet"},
	{"Unused Safe House"},
	{"RC Battlefield"},
	{"Barber 1"},
	{"Barber 2"},
	{"Barber 3"},
	{"Tatoo parlour 1"},
	{"Tatoo parlour 2"},
	{"Tatoo parlour 3"},
	{"LS Police HQ"},
	{"SF Police HQ"},
	{"LV Police HQ"},
	{"Car School"},
	{"8-Track"},
	{"Bloodbowl"},
	{"Dirt Track"},
	{"Kickstart"},
	{"Vice Stadium"},
	{"SF Garage"},
	{"LS Garage"},
	{"SF Bomb Shop"},
	{"Blueberry Warehouse"},
	{"LV Warehouse 1"},
	{"LV Warehouse 2"},
	{"Catigula's Hidden Room"},
	{"Bank"},
	{"Bank - Behind Desk"},
	{"LS Atruim"},
	{"Bike School"}
};

public OnFilterScriptInit()
{
	print("\n--------------------------------------");
	print(" OrTeles Filterscript by Orion");
	print("--------------------------------------\n");
	return 1;
}

public OnFilterScriptExit()
{
	return 1;
}

public OnPlayerCommandText(playerid, cmdtext[])
{
	if(!strcmp(cmdtext, #/telemenu, true))
	{
	    if(IsPlayerConnected(playerid))
    	{
    	    new
    	        telestring[1550],
    	        part[40];
	    	#if AdminOnly
	    	if(IsPlayerAdmin(playerid))
	    	{
	    	#endif
	    	format(telestring, sizeof(telestring), "{FFFFFF}%s", OrTeleports[0]);
	    	for(new OrT = 1; OrT < sizeof(OrTeleports); OrT++)
	    	{
	    	    format(part, sizeof(part), "\n%s", OrTeleports[OrT]);
	    	    strcat(telestring, part, sizeof(telestring));
			}
			ShowPlayerDialog(playerid, DIALOGID, DIALOG_STYLE_LIST, "Select a teleport location", telestring, "Teleport", "Cancel");
			#if AdminOnly
			}
			else
			{
			    SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have don't have the rights to use this command!");
			    return 1;
			}
			#endif
		}
		return 1;
	}
	return 0;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	if(dialogid == DIALOGID)
	{
	    if(!response)
	    {
	        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have canceled the teleport.");
	        return 1;
		}
		switch(listitem)
		{
		    case 0:
		    {
		        SetPlayerPos(playerid, -25.884498,-185.868988,1003.546875);
		        SetPlayerInterior(playerid, 17);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}24/7 1");
			    return 1;
		    }
		    case 1:
		    {
		        SetPlayerPos(playerid, 6.091179,-29.271898,1003.549438);
		        SetPlayerInterior(playerid, 10);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}24/7 2");
			    return 1;
		    }
		    case 2:
		    {
		        SetPlayerPos(playerid, -30.946699,-89.609596,1003.546875);
		        SetPlayerInterior(playerid, 18);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}24/7 3");
			    return 1;
		    }
		    case 3:
		    {
		        SetPlayerPos(playerid, -25.132598,-139.066986,1003.546875);
		        SetPlayerInterior(playerid, 16);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}24/7 4");
			    return 1;
		    }
		    case 4:
		    {
		        SetPlayerPos(playerid, -27.312299,-29.277599,1003.557250);
		        SetPlayerInterior(playerid, 4);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}24/7 5");
			    return 1;
		    }
		    case 5:
		    {
		        SetPlayerPos(playerid, -26.691598,-55.714897,1003.546875);
		        SetPlayerInterior(playerid, 6);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}24/7 6");
			    return 1;
		    }
		    case 6:
		    {
		        SetPlayerPos(playerid, -1827.147338,7.207417,1061.143554);
		        SetPlayerInterior(playerid, 14);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Airoport Ticket Desk");
			    return 1;
		    }
		    case 7:
		    {
		        SetPlayerPos(playerid, -1861.936889,54.908092,1061.143554);
		        SetPlayerInterior(playerid, 14);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Airoport Baggage Reclaim");
			    return 1;
		    }
		    case 8:
		    {
		        SetPlayerPos(playerid, 1.808619,32.384357,1199.593750);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Shamal");
			    return 1;
		    }
		    case 9:
		    {
		        SetPlayerPos(playerid, 315.745086,984.969299,1958.919067);
		        SetPlayerInterior(playerid, 9);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Andromada");
			    return 1;
		    }
		    case 10:
		    {
		        SetPlayerPos(playerid, 286.148986,-40.644397,1001.515625);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Ammunation 1");
			    return 1;
		    }
		    case 11:
		    {
		        SetPlayerPos(playerid, 286.800994,-82.547599,1001.515625);
		        SetPlayerInterior(playerid, 4);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Ammunation 2");
			    return 1;
		    }
		    case 12:
		    {
		        SetPlayerPos(playerid, 296.919982,-108.071998,1001.515625);
		        SetPlayerInterior(playerid, 6);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Ammunation 3");
			    return 1;
		    }
		    case 13:
		    {
		        SetPlayerPos(playerid, 314.820983,-141.431991,999.601562);
		        SetPlayerInterior(playerid, 7);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Ammunation 4");
			    return 1;
		    }
		    case 14:
		    {
		        SetPlayerPos(playerid, 316.524993,-167.706985,999.593750);
		        SetPlayerInterior(playerid, 6);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Ammunation 5");
			    return 1;
		    }
		    case 15:
		    {
		        SetPlayerPos(playerid, 302.292877,-143.139099,1004.062500);
		        SetPlayerInterior(playerid, 7);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Ammunation Booths");
			    return 1;
		    }
		    case 16:
		    {
		        SetPlayerPos(playerid, 298.507934,-141.647048,1004.054748);
		        SetPlayerInterior(playerid, 7);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Ammunation Range");
			    return 1;
		    }
		    case 17:
		    {
		        SetPlayerPos(playerid, 1038.531372,0.111030,1001.284484);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Blastin Fools Hallway");
			    return 1;
		    }
		    case 18:
		    {
		        SetPlayerPos(playerid, 444.646911,508.239044,1001.419494);
		        SetPlayerInterior(playerid, 12);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Budget Inn Motel Room");
			    return 1;
		    }
		    case 19:
		    {
		        SetPlayerPos(playerid, 2215.454833,-1147.475585,1025.796875);
		        SetPlayerInterior(playerid, 14);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Jefferson Motel");
			    return 1;
		    }
		    case 20:
		    {
		        SetPlayerPos(playerid, 833.269775,10.588416,1004.179687);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Off Track Betting Shop");
			    return 1;
		    }
		    case 21:
		    {
		        SetPlayerPos(playerid, -103.559165,-24.225606,1000.718750);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Sex Shop");
			    return 1;
		    }
		    case 22:
		    {
		        SetPlayerPos(playerid, 963.418762,2108.292480,1011.030273);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Meat Factory");
			    return 1;
		    }
		    case 23:
		    {
		        SetPlayerPos(playerid, -2240.468505,137.060440,1035.414062);
		        SetPlayerInterior(playerid, 6);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Zero's RC Shop");
			    return 1;
		    }
		    case 24:
		    {
		        SetPlayerPos(playerid, 663.836242,-575.605407,16.343263);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Dillmore Gas Station");
			    return 1;
		    }
		    case 25:
		    {
		        SetPlayerPos(playerid, 2169.461181,1618.798339,999.976562);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Catigula's Basement");
			    return 1;
		    }
			case 26:
		    {
		        SetPlayerPos(playerid, 1889.953369,1017.438293,31.882812);
		        SetPlayerInterior(playerid, 10);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}FDC Janitors Room");
			    return 1;
		    }
		    case 27:
		    {
		        SetPlayerPos(playerid, -2159.122802,641.517517,1052.381713);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Woozie's Office");
			    return 1;
		    }
		    case 28:
		    {
		        SetPlayerPos(playerid, 207.737991,-109.019996,1005.132812);
		        SetPlayerInterior(playerid, 15);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Binco");
			    return 1;
		    }
		    case 29:
		    {
		        SetPlayerPos(playerid, 204.332992,-166.694992,1000.523437);
		        SetPlayerInterior(playerid, 14);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Didier Sachs");
			    return 1;
		    }
		    case 30:
		    {
		        SetPlayerPos(playerid, 207.054992,-138.804992,1003.507812);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Prolaps");
			    return 1;
		    }
		    case 31:
		    {
		        SetPlayerPos(playerid, 203.777999,-48.492397,1001.804687);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Suburban");
			    return 1;
		    }
		    case 32:
		    {
		        SetPlayerPos(playerid, 226.293991,-7.431529,1002.210937);
		        SetPlayerInterior(playerid, 5);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Victim");
			    return 1;
		    }
		    case 33:
		    {
		        SetPlayerPos(playerid, 161.391006,-93.159156,1001.804687);
		        SetPlayerInterior(playerid, 18);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}ZIP");
			    return 1;
		    }
		    case 34:
		    {
		        SetPlayerPos(playerid, 493.390991,-22.722799,1000.679687);
		        SetPlayerInterior(playerid, 17);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Alhambra");
			    return 1;
		    }
		    case 35:
		    {
		        SetPlayerPos(playerid, 501.980987,-69.150199,998.757812);
		        SetPlayerInterior(playerid, 11);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Ten Green Bottles");
			    return 1;
		    }
		    case 36:
		    {
		        SetPlayerPos(playerid, -227.027999,1401.229980,27.765625);
		        SetPlayerInterior(playerid, 18);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Lil' Probe Inn");
			    return 1;
		    }
		    case 37:
		    {
		        SetPlayerPos(playerid, 457.304748,-88.428497,999.554687);
		        SetPlayerInterior(playerid, 4);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Jay's Dinner");
			    return 1;
		    }
		    case 38:
		    {
		        SetPlayerPos(playerid, 454.973937,-110.104995,1000.077209);
		        SetPlayerInterior(playerid, 5);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Gant Bridge Dinner");
			    return 1;
		    }
		    case 39:
		    {
		        SetPlayerPos(playerid, 435.271331,-80.958938,999.554687);
		        SetPlayerInterior(playerid, 6);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Secret Valley Dinner");
			    return 1;
		    }
		    case 40:
		    {
		        SetPlayerPos(playerid, 452.489990,-18.179698,1001.132812);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}World of Coq");
			    return 1;
		    }
		    case 41:
		    {
		        SetPlayerPos(playerid, 681.557861,-455.680053,-25.609874);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Welcome Pump");
			    return 1;
		    }
		    case 42:
		    {
		        SetPlayerPos(playerid, 375.962463,-65.816848,1001.507812);
		        SetPlayerInterior(playerid, 10);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Burger Shot");
			    return 1;
		    }
		    case 43:
		    {
		        SetPlayerPos(playerid, 369.579528,-4.487294,1001.858886);
		        SetPlayerInterior(playerid, 9);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Cluckin' Bell");
			    return 1;
		    }
		    case 44:
		    {
		        SetPlayerPos(playerid, 373.825653,-117.270904,1001.499511);
		        SetPlayerInterior(playerid, 5);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Well Stacked Pizza");
			    return 1;
		    }
		    case 45:
		    {
		        SetPlayerPos(playerid, 381.169189,-188.803024,1000.632812);
		        SetPlayerInterior(playerid, 17);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Jimmy's Sticky Ring");
			    return 1;
		    }
		    case 46:
		    {
		        SetPlayerPos(playerid, 244.411987,305.032989,999.148437);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Denise Room");
			    return 1;
		    }
		    case 47:
		    {
		        SetPlayerPos(playerid, 271.884979,306.631988,999.148437);
		        SetPlayerInterior(playerid, 2);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Katie Room");
			    return 1;
		    }
		    case 48:
		    {
		        SetPlayerPos(playerid, 291.282989,310.031982,999.148437);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Helena Room");
			    return 1;
		    }
		    case 49:
		    {
		        SetPlayerPos(playerid, 302.180999,300.722991,999.148437);
		        SetPlayerInterior(playerid, 4);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Michelle Room");
			    return 1;
		    }
		    case 50:
		    {
		        SetPlayerPos(playerid, 322.197998,302.497985,999.148437);
		        SetPlayerInterior(playerid, 5);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Barbara Room");
			    return 1;
		    }
		    case 51:
		    {
		        SetPlayerPos(playerid, 346.870025,309.259033,999.155700);
		        SetPlayerInterior(playerid, 6);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Millie Room");
			    return 1;
		    }
		    case 52:
		    {
		        SetPlayerPos(playerid, -959.564392,1848.576782,9.000000);
		        SetPlayerInterior(playerid, 17);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Sherman Dam");
			    return 1;
		    }
		    case 53:
		    {
		        SetPlayerPos(playerid, 384.808624,173.804992,1008.382812);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Planning Dept.");
			    return 1;
		    }
		    case 54:
		    {
		        SetPlayerPos(playerid, 223.431976,1872.400268,13.734375);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Area 51");
			    return 1;
		    }
		    case 55:
		    {
		        SetPlayerPos(playerid, 772.111999,-3.898649,1000.728820);
		        SetPlayerInterior(playerid, 5);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}LS Gym");
			    return 1;
		    }
		    case 56:
		    {
		        SetPlayerPos(playerid, 774.213989,-48.924297,1000.585937);
		        SetPlayerInterior(playerid, 6);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}SF Gym");
			    return 1;
		    }
		    case 57:
		    {
		        SetPlayerPos(playerid, 773.579956,-77.096694,1000.655029);
		        SetPlayerInterior(playerid, 7);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}LV Gym");
			    return 1;
		    }
		    case 58:
		    {
		        SetPlayerPos(playerid, 1527.229980,-11.574499,1002.097106);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}B Dup's House");
			    return 1;
		    }
		    case 59:
		    {
		        SetPlayerPos(playerid, 1523.509887,-47.821197,1002.130981);
		        SetPlayerInterior(playerid, 2);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}B Dup's Crack Pad");
			    return 1;
		    }
		    case 60:
		    {
		        SetPlayerPos(playerid, 2496.049804,-1695.238159,1014.742187);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}CJ's House");
			    return 1;
		    }
		    case 61:
		    {
		        SetPlayerPos(playerid, 1267.663208,-781.323242,1091.906250);
		        SetPlayerInterior(playerid, 5);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Madd Dogg's Mansion");
			    return 1;
		    }
		    case 62:
		    {
		        SetPlayerPos(playerid, 513.882507,-11.269994,1001.565307);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}OG Loc's House");
			    return 1;
		    }
		    case 63:
		    {
		        SetPlayerPos(playerid, 2454.717041,-1700.871582,1013.515197);
		        SetPlayerInterior(playerid, 2);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Ryder's House");
			    return 1;
		    }
		    case 64:
		    {
		        SetPlayerPos(playerid, 2527.654052,-1679.388305,1015.498596);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Sweet's House");
			    return 1;
		    }
		    case 65:
		    {
		        SetPlayerPos(playerid, 2543.462646,-1308.379882,1026.728393);
		        SetPlayerInterior(playerid, 2);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Crack Factory");
			    return 1;
		    }
		    case 66:
		    {
		        SetPlayerPos(playerid, 1212.019897,-28.663099,1000.953125);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Big Spread Ranch");
			    return 1;
		    }
		    case 67:
		    {
		        SetPlayerPos(playerid, 761.412963,1440.191650,1102.703125);
		        SetPlayerInterior(playerid, 6);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Fanny Batters");
			    return 1;
		    }
		    case 68:
		    {
		        SetPlayerPos(playerid, 1204.809936,-11.586799,1000.921875);
		        SetPlayerInterior(playerid, 2);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Strip Club");
			    return 1;
		    }
		    case 69:
		    {
		        SetPlayerPos(playerid, 1204.809936,13.897239,1000.921875);
		        SetPlayerInterior(playerid, 2);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Strip Club Private Room");
			    return 1;
		    }
		    case 70:
		    {
		        SetPlayerPos(playerid, 942.171997,-16.542755,1000.929687);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Unnamed Brothel");
			    return 1;
		    }
		    case 71:
		    {
		        SetPlayerPos(playerid, 964.106994,-53.205497,1001.124572);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Tiger Skin Brothel");
			    return 1;
		    }
		    case 72:
		    {
		        SetPlayerPos(playerid, -2640.762939,1406.682006,906.460937);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Pleasure Domes");
			    return 1;
		    }
		    case 73:
		    {
		        SetPlayerPos(playerid, -729.276000,503.086944,1371.971801);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Liberty City Outside");
			    return 1;
		    }
		    case 74:
		    {
		        SetPlayerPos(playerid, -794.806396,497.738037,1376.195312);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Liberty City Inside");
			    return 1;
		    }
		    case 75:
		    {
		        SetPlayerPos(playerid, 	2350.339843,-1181.649902,1027.976562);
		        SetPlayerInterior(playerid, 5);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Gang House");
			    return 1;
		    }
		    case 76:
		    {
		        SetPlayerPos(playerid, 2807.619873,-1171.899902,1025.570312);
		        SetPlayerInterior(playerid, 8);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Colonel Furhberger's House");
			    return 1;
		    }
		    case 77:
		    {
		        SetPlayerPos(playerid, 18.564971,1118.209960,1083.882812);
		        SetPlayerInterior(playerid, 5);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Crack Den");
			    return 1;
		    }
		    case 78:
		    {
		        SetPlayerPos(playerid, 1412.639892,-1.787510,1000.924377);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Warehouse 1");
			    return 1;
		    }
		    case 79:
		    {
		        SetPlayerPos(playerid, 1302.519897,-1.787510,1001.028259);
		        SetPlayerInterior(playerid, 18);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Warehouse 2");
			    return 1;
		    }
		    case 80:
		    {
		        SetPlayerPos(playerid, 2522.000000,-1673.383911,14.866223);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Sweet's Garage");
			    return 1;
		    }
		    case 81:
		    {
		        SetPlayerPos(playerid, -221.059051,1408.984008,27.773437);
		        SetPlayerInterior(playerid, 18);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Lil' Probe Inn Toilet");
			    return 1;
		    }
		    case 82:
		    {
		        SetPlayerPos(playerid, 2324.419921,-1145.568359,1050.710083);
		        SetPlayerInterior(playerid, 12);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Unused Safe House");
			    return 1;
		    }
		    case 83:
		    {
		        SetPlayerPos(playerid, -975.975708,1060.983032,1345.671875);
		        SetPlayerInterior(playerid, 10);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}RC Battlefield");
			    return 1;
		    }
		    case 84:
		    {
		        SetPlayerPos(playerid, 411.625976,-21.433298,1001.804687);
		        SetPlayerInterior(playerid, 2);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Barber 1");
			    return 1;
		    }
		    case 85:
		    {
		        SetPlayerPos(playerid, 418.652984,-82.639793,1001.804687);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Barber 2");
			    return 1;
		    }
		    case 86:
		    {
		        SetPlayerPos(playerid, 412.021972,-52.649898,1001.898437);
		        SetPlayerInterior(playerid, 12);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Barber 3");
			    return 1;
		    }
		    case 87:
		    {
		        SetPlayerPos(playerid, -204.439987,-26.453998,1002.273437);
		        SetPlayerInterior(playerid, 16);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Tatoo Parlour 1");
			    return 1;
		    }
		    case 88:
		    {
		        SetPlayerPos(playerid, -204.439987,-8.469599,1002.273437);
		        SetPlayerInterior(playerid, 17);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Tatoo Parlour 2");
			    return 1;
		    }
		    case 89:
		    {
		        SetPlayerPos(playerid, -204.439987,-43.652496,1002.273437);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Tatoo Parlour 3");
			    return 1;
		    }
		    case 90:
		    {
		        SetPlayerPos(playerid, 246.783996,63.900199,1003.640625);
		        SetPlayerInterior(playerid, 6);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}LS Police HQ");
			    return 1;
		    }
		    case 91:
		    {
		        SetPlayerPos(playerid, 246.375991,109.245994,1003.218750);
		        SetPlayerInterior(playerid, 10);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}SF police HQ");
			    return 1;
		    }
		    case 92:
		    {
		        SetPlayerPos(playerid, 288.745971,169.350997,1007.171875);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}LV police HQ");
			    return 1;
		    }
		    case 93:
		    {
		        SetPlayerPos(playerid, -2029.798339,-106.675910,1035.171875);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Car School");
			    return 1;
		    }
		    case 94:
		    {
		        SetPlayerPos(playerid, -1398.065307,-217.028900,1051.115844);
		        SetPlayerInterior(playerid, 7);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}8-Track");
			    return 1;
		    }
		    case 95:
		    {
		        SetPlayerPos(playerid, -1398.103515,937.631164,1036.479125);
		        SetPlayerInterior(playerid, 15);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Bloodbowl");
			    return 1;
		    }
		    case 96:
		    {
		        SetPlayerPos(playerid, -1444.645507,-664.526000,1053.572998);
		        SetPlayerInterior(playerid, 4);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Dirt Track");
			    return 1;
		    }
		    case 97:
		    {
		        SetPlayerPos(playerid, -1465.268676,1557.868286,1052.531250);
		        SetPlayerInterior(playerid, 14);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Kickstart");
			    return 1;
		    }
		    case 98:
		    {
		        SetPlayerPos(playerid, -1401.829956,107.051300,1032.273437);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Vice Stadium");
			    return 1;
		    }
		    case 99:
		    {
		        SetPlayerPos(playerid, -1790.378295,1436.949829,7.187500);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}SF Garage");
			    return 1;
		    }
		    case 100:
		    {
		        SetPlayerPos(playerid, 1643.839843,-1514.819580,13.566620);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}LS Garage");
			    return 1;
		    }
		    case 101:
		    {
		        SetPlayerPos(playerid, -1685.636474,1035.476196,45.210937);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}SF Bomb Shop");
			    return 1;
		    }
		    case 102:
		    {
		        SetPlayerPos(playerid, 76.632553,-301.156829,1.578125);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Blueberry Warehouse");
			    return 1;
		    }
		    case 103:
		    {
		        SetPlayerPos(playerid, 1059.895996,2081.685791,10.820312);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}LV Warehouse 1");
			    return 1;
		    }
		    case 104:
		    {
		        SetPlayerPos(playerid, 1059.180175,2148.938720,10.820312);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}LV Warehouse 2");
			    return 1;
		    }
		    case 105:
		    {
		        SetPlayerPos(playerid, 2131.507812,1600.818481,1008.359375);
		        SetPlayerInterior(playerid, 1);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Catigula's Hidden Room");
			    return 1;
		    }
		    case 106:
		    {
		        SetPlayerPos(playerid, 2315.952880,-1.618174,26.742187);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Bank");
			    return 1;
		    }
		    case 107:
		    {
		        SetPlayerPos(playerid, 2319.714843,-14.838361,26.749565);
		        SetPlayerInterior(playerid, 0);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Bank - Behind Desk");
			    return 1;
		    }
		    case 108:
		    {
		        SetPlayerPos(playerid, 1710.433715,-1669.379272,20.225049);
		        SetPlayerInterior(playerid, 18);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}LS Atruim");
			    return 1;
		    }
		    case 109:
		    {
		        SetPlayerPos(playerid, 1494.325195,1304.942871,1093.289062);
		        SetPlayerInterior(playerid, 3);
		        SendClientMessage(playerid, 0xFF0000FF, "Or{FFFFFF}Teles: {B4B5B7}You have been teleported to {0099FF}Bike School");
			    return 1;
		    }
		}
	}
	return 1;
}