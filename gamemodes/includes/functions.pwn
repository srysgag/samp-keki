//--------------------------------[FUNCTIONS.PWN]--------------------------------
/*
	- Weapon Function
	- Player Function
	- Vehicle Function
	- MATH
	- DATE/TIME
	- Message
	- Regex
	- MySQL
*/

stock ClearChatBox(playerid) for (new i = 0; i < 100; i ++) SendClientMessage(playerid, -1, "");

stock RemoveFromVehicle(playerid)
{
	if (IsPlayerInAnyVehicle(playerid))
	{
		new
		    Float:fX,
	    	Float:fY,
	    	Float:fZ;

		GetPlayerPos(playerid, fX, fY, fZ);
		SetPlayerPos(playerid, fX, fY, fZ + 1.5);
	}
	return 1;
}

//=========================== [ Weapon Function ] ========================

stock ReturnWeaponNameEx(weaponid)
{
	new
		name[24];
		
	switch(weaponid) {
		case 0: name = "Punch";
		case 30: name = "AK-47";
		default: {
			GetWeaponName(weaponid, name, sizeof(name));	
		}
	}
	return name;
}

stock ReturnFactionWeaponName(weaponid)
{
	static
		name[24];

	if (weaponid == 100) {
	    name = "Health";
	}
	else if (weaponid == 200) {
	    name = "Armour";
	}
	else {
		if (weaponid == 0) name = "��ҧ";
		else GetWeaponName(weaponid, name, sizeof(name));
	}
	return name;
}

stock IsBulletWeapon(weaponid)
{
	return (WEAPON_COLT45 <= weaponid <= WEAPON_SNIPER) || weaponid == WEAPON_MINIGUN;
}

stock IsMeleeWeapon(weaponid)
{
	return (WEAPON_UNARMED <= weaponid <= WEAPON_KATANA) || (WEAPON_DILDO <= weaponid <= WEAPON_CANE) || weaponid == WEAPON_PISTOLWHIP;
}

stock SetPlayerWeaponSkill(playerid, skill) {
	switch(skill) {
	    case NORMAL_SKILL: {
            for(new i = 0; i != 11;++i) SetPlayerSkillLevel(playerid, i, 200);
            SetPlayerSkillLevel(playerid, 0, 40);
            SetPlayerSkillLevel(playerid, 6, 50);
	    }
	    case MEDIUM_SKILL: {
            for(new i = 0; i != 11;++i) SetPlayerSkillLevel(playerid, i, 500);
            SetPlayerSkillLevel(playerid, 0, 500);
            SetPlayerSkillLevel(playerid, 6, 500);
	    }
	    case FULL_SKILL: {
            for(new i = 0; i != 11;++i) SetPlayerSkillLevel(playerid, i, 999);
            SetPlayerSkillLevel(playerid, 0, 998);
            SetPlayerSkillLevel(playerid, 6, 998);
	    }
	}
}
//=========================== [ Weapon Function ] ========================

//=========================== [ Vehicle Function ] ========================

ReturnVehicleModelName(model)
{
	new
	    name[32] = "None";

    if (model < 400 || model > 611)
	    return name;

	format(name, sizeof(name), g_arrVehicleNames[model - 400]);
	return name;
}

GetVehicleModelByName(const name[])
{
	if (IsNumeric(name) && (strval(name) >= 400 && strval(name) <= 611))
	    return strval(name);

	for (new i = 0; i < sizeof(g_arrVehicleNames); i ++)
	{
	    if (strfind(g_arrVehicleNames[i], name, true) != -1)
	    {
	        return i + 400;
		}
	}
	return 0;
}
//=========================== [ Vehicle Function ] ========================

//=========================== [ Player Function ] ========================
stock IsPlayerNearPlayer(playerid, targetid, Float:radius)
{
	static
		Float:fX,
		Float:fY,
		Float:fZ;

	GetPlayerPos(targetid, fX, fY, fZ);

	return (GetPlayerInterior(playerid) == GetPlayerInterior(targetid) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(targetid) && playerData[targetid][pSpectating] == INVALID_PLAYER_ID) && IsPlayerInRangeOfPoint(playerid, radius, fX, fY, fZ);
}

stock Float:AngleBetweenPoints(Float:x1, Float:y1, Float:x2, Float:y2)
{
	return -(90.0 - atan2(y1 - y2, x1 - x2));
}

stock MakePlayerFacePlayer(playerid, targetid, opposite = false)
{
	new Float:x1, Float:y1, Float:z1;
	new Float:x2, Float:y2, Float:z2;

	GetPlayerPos(playerid, x1, y1, z1);
	GetPlayerPos(targetid, x2, y2, z2);
	new Float:angle = AngleBetweenPoints(x2, y2, x1, y1);

	if (opposite) {
		angle += 180.0;
		if (angle > 360.0) angle -= 360.0;
	}

	if (angle < 0.0) angle += 360.0;
	if (angle > 360.0) angle -= 360.0;

	SetPlayerFacingAngle(playerid, angle);
}

stock IsPlayerBehindPlayer(playerid, targetid, Float:diff = 90.0)
{
	new Float:x1, Float:y1, Float:z1;
	new Float:x2, Float:y2, Float:z2;
	new Float:ang, Float:angdiff;

	GetPlayerPos(playerid, x1, y1, z1);
	GetPlayerPos(targetid, x2, y2, z2);
	GetPlayerFacingAngle(targetid, ang);

	angdiff = AngleBetweenPoints(x1, y1, x2, y2);

	if (angdiff < 0.0) angdiff += 360.0;
	if (angdiff > 360.0) angdiff -= 360.0;

	ang = ang - angdiff;

	if (ang > 180.0) ang -= 360.0;
	if (ang < -180.0) ang += 360.0;

	return floatabs(ang) > diff;
}

stock ProxDetector(playerid, Float:radius, const str[])
{
	new Float:posx, Float:posy, Float:posz;
	new Float:oldposx, Float:oldposy, Float:oldposz;
	new Float:tempposx, Float:tempposy, Float:tempposz;

	GetPlayerPos(playerid, oldposx, oldposy, oldposz);

	foreach (new i : Player)
	{
		if(GetPlayerInterior(playerid) == GetPlayerInterior(i) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i))
		{
			GetPlayerPos(i, posx, posy, posz);
			tempposx = (oldposx -posx);
			tempposy = (oldposy -posy);
			tempposz = (oldposz -posz);

			if (((tempposx < radius/16) && (tempposx > -radius/16)) && ((tempposy < radius/16) && (tempposy > -radius/16)) && ((tempposz < radius/16) && (tempposz > -radius/16)))
			{
				SendClientMessage(i, COLOR_GRAD1, str);
			}
			else if (((tempposx < radius/8) && (tempposx > -radius/8)) && ((tempposy < radius/8) && (tempposy > -radius/8)) && ((tempposz < radius/8) && (tempposz > -radius/8)))
			{
				SendClientMessage(i, COLOR_GRAD2, str);
			}
			else if (((tempposx < radius/4) && (tempposx > -radius/4)) && ((tempposy < radius/4) && (tempposy > -radius/4)) && ((tempposz < radius/4) && (tempposz > -radius/4)))
			{
				SendClientMessage(i, COLOR_GRAD3, str);
			}
			else if (((tempposx < radius/2) && (tempposx > -radius/2)) && ((tempposy < radius/2) && (tempposy > -radius/2)) && ((tempposz < radius/2) && (tempposz > -radius/2)))
			{
				SendClientMessage(i, COLOR_GRAD4, str);
			}
			else if (((tempposx < radius) && (tempposx > -radius)) && ((tempposy < radius) && (tempposy > -radius)) && ((tempposz < radius) && (tempposz > -radius)))
			{
				SendClientMessage(i, COLOR_GRAD5, str);
			}
		}
	}
	return 1;
}

stock ChatAnimation(playerid, length)
{
	if(GetPlayerState(playerid) == PLAYER_STATE_ONFOOT && !playingAnimation{playerid})
	{
		if(isDeathmode{playerid} || isInjuredmode{playerid} || IsPlayerInAnyVehicle(playerid) || GetPlayerSpecialAction(playerid) != SPECIAL_ACTION_NONE) return 1;

		new chatstyle = playerData[playerid][pTalkStyle];
		playingAnimation{playerid}=true;
		if(chatstyle == 0) { ApplyAnimation(playerid,"PED","IDLE_CHAT",4.1,1,0,0,1,1); }
		else if(chatstyle == 1) { ApplyAnimation(playerid,"GANGS","prtial_gngtlkA",4.1,1,0,0,1,1); }
		else if(chatstyle == 2) { ApplyAnimation(playerid,"GANGS","prtial_gngtlkB",4.1,1,0,0,1,1); }
		else if(chatstyle == 3) { ApplyAnimation(playerid,"GANGS","prtial_gngtlkC",4.1,1,0,0,1,1);}
		else if(chatstyle == 4) { ApplyAnimation(playerid,"GANGS","prtial_gngtlkD",4.1,1,0,0,1,1);}
		else if(chatstyle == 5) { ApplyAnimation(playerid,"GANGS","prtial_gngtlkE",4.1,1,0,0,1,1);}
		else if(chatstyle == 6) { ApplyAnimation(playerid,"GANGS","prtial_gngtlkF",4.1,1,0,0,1,1);}
		else if(chatstyle == 7) { ApplyAnimation(playerid,"GANGS","prtial_gngtlkG",4.1,1,0,0,1,1);}
		else if(chatstyle == 8) { ApplyAnimation(playerid,"GANGS","prtial_gngtlkH",4.1,1,0,0,1,1);}
		SetTimerEx("StopChatting", floatround(length)*100, 0, "i", playerid);
	}
	return 1;
}

forward StopChatting(playerid);
public StopChatting(playerid) ApplyAnimation(playerid, "CARRY", "crry_prtial", 4.0, 0, 0, 0, 0, 0), playingAnimation{playerid}=false;

//=========================== [ Player Function ] ========================

stock GetBodyPartName(bodypart)
{
	new part[11];
	switch(bodypart)
	{
		case BODY_PART_TORSO: part = "�ӵ��";
		case BODY_PART_GROIN: part = "��˹պ";
		case BODY_PART_LEFT_ARM: part = "ᢹ����";
		case BODY_PART_RIGHT_ARM: part = "ᢹ���";
		case BODY_PART_LEFT_LEG: part = "�ҫ���";
		case BODY_PART_RIGHT_LEG: part = "�Ң��";
		case BODY_PART_HEAD: part = "���";
		default: part = "����Һ";
	}
	return part;
}

stock GetDynamicPlayerPos(playerid, &Float:x, &Float:y, &Float:z)
{
	new world = GetPlayerVirtualWorld(playerid);
	if(world == 0)
	{
	    GetPlayerPos(playerid, x, y, z);
	}
	else
	{
		/* House & Business, HQ Faction... */
		GetPlayerPos(playerid, x, y, z);
	}
	return 1;
}

//==================== [ MATH ] ==========================
stock IsNumeric(const str[])
{
	for (new i = 0, l = strlen(str); i != l; i ++)
	{
	    if (i == 0 && str[0] == '-')
			continue;

	    else if (str[i] < '0' || str[i] > '9')
			return 0;
	}
	return 1;
}

stock Float:GetPointDistanceToPoint(Float:x1,Float:y1,Float:x2,Float:y2)
{
  new Float:x, Float:y;
  x = x1-x2;
  y = y1-y2;
  return floatsqroot(x*x+y*y);
}

//==================== [ DATE/TIME ] ==========================

stock ReturnDateTime(type = 0)
{
 	new
	    szDay[80],
		date[6];

	getdate(date[2], date[1], date[0]);
	gettime(date[3], date[4], date[5]);

	switch(type) {
		case 0: format(szDay, sizeof(szDay), "%02d %s �.� %d ���� %02d:%02d:%02d", date[0], MonthDay[date[1] - 1], date[2]+543, date[3], date[4], date[5]);
		case 1: format(szDay, sizeof(szDay), "%02d-%02d-%d %02d:%02d", date[0], date[1], date[2]+543, date[3], date[4]);
		case 2: format(szDay, sizeof(szDay), "%02d %s %d ���� %02d:%02d", date[0], szMonthDay[date[1] - 1], date[2]+543, date[3], date[4]);
	}

	return szDay;
}
/*
stock returnOrdinal(number)
{
	new
	    ordinal[4][3] = { "st", "nd", "rd", "th" }
	;
	number = number < 0 ? -number : number;
	return (((10 < (number % 100) < 14)) ? ordinal[3] : (0 < (number % 10) < 4) ? ordinal[((number % 10) - 1)] : ordinal[3]);
}*/

//==================== [ Message ] ==========================
stock SendNearbyMessage(playerid, Float:radius, color, const str[], {Float,_}:...)
{
	static
	    args,
	    start,
	    end,
	    string[144]
	;
	#emit LOAD.S.pri 8
	#emit STOR.pri args

	if (args > 16)
	{
		#emit ADDR.pri str
		#emit STOR.pri start

	    for (end = start + (args - 16); end > start; end -= 4)
		{
	        #emit LREF.pri end
	        #emit PUSH.pri
		}
		#emit PUSH.S str
		#emit PUSH.C 144
		#emit PUSH.C string

		#emit LOAD.S.pri 8
		#emit CONST.alt 4
		#emit SUB
		#emit PUSH.pri

		#emit SYSREQ.C format
		#emit LCTRL 5
		#emit SCTRL 4

        foreach (new i : Player)
		{
			if (IsPlayerNearPlayer(i, playerid, radius)) {
  				SendClientMessage(i, color, string);
			}
		}
		return 1;
	}
	foreach (new i : Player)
	{
		if (IsPlayerNearPlayer(i, playerid, radius)) {
			SendClientMessage(i, color, str);
		}
	}
	return 1;
}

stock SendAdminMessage(color, const text[], {Float, _}:...)
{
	static
	    args,
	    str[144];

	if ((args = numargs()) == 2)
	{
        foreach (new i : Player)
		{
			if (playerData[i][pAdmin]) {
  				SendClientMessage(i, color, text);
			}
		}
	}
	else
	{
		while (--args >= 2)
		{
			#emit LCTRL 5
			#emit LOAD.alt args
			#emit SHL.C.alt 2
			#emit ADD.C 12
			#emit ADD
			#emit LOAD.I
			#emit PUSH.pri
		}
		#emit PUSH.S text
		#emit PUSH.C 144
		#emit PUSH.C str
		#emit LOAD.S.pri 8
		#emit ADD.C 4
		#emit PUSH.pri
		#emit SYSREQ.C format
		#emit LCTRL 5
		#emit SCTRL 4

        foreach (new i : Player)
		{
			if (playerData[i][pAdmin]) {
  				SendClientMessage(i, color, str);
			}
		}

		#emit RETN
	}
	return 1;
}
//==================== [ Regex ] ==========================
/*
stock IsEnglishAndNumber(nickname[])
{
    new Regex:r = Regex_New("[a-zA-Z0-9 _]+");

    new check = Regex_Check(nickname, r);

    Regex_Delete(r);

    return check;
}*/

//==================== [ MySQL ] ==========================


MySQLUpdateInit(table_name[], column_name[], type = MYSQLUPDATE_TYPE_SINGLE) { // by Aktah
	SetGVarString("MYSQL_UpdateTableName", table_name);
	SetGVarString("MYSQL_UpdateColumnName", column_name);
	SetGVarInt("MYSQL_UpdateType", type);
}

MySQLUpdateBuild(query[], sqlid) // by Aktah
{
	new table_name[64], column_name[64];
	GetGVarString("MYSQL_UpdateTableName", table_name, 64);
	GetGVarString("MYSQL_UpdateColumnName", column_name, 64);

	new querylen = strlen(query);

	new querymax = MAX_STRING;
	if (querylen < 1) format(query, querymax, "UPDATE `%s` SET ", table_name);
	else if (querymax-querylen < 128) // make sure we're being safe here
	{
		// query is too large, send this one and reset
		new whereclause[64];
		format(whereclause, sizeof(whereclause), " WHERE `%s`=%d", column_name, sqlid);
		strcat(query, whereclause, querymax);

		if(GetGVarInt("MYSQL_UpdateType")==MYSQLUPDATE_TYPE_SINGLE) mysql_query(dbCon, query);
		else mysql_tquery(dbCon, query);

		format(query, querymax, "UPDATE `%s` SET ", table_name);
	}
	else if (strfind(query, "=", true) != -1) strcat(query, ",", MAX_STRING);
	return 1;
}

MySQLUpdateFinish(query[], sqlid) // by Aktah
{
	new whereclause[64], column_name[64], type = GetGVarInt("MYSQL_UpdateType");
	GetGVarString("MYSQL_UpdateColumnName", column_name, 64);
	format(whereclause, sizeof(whereclause), "WHERE `%s`=", column_name);
	if (strcmp(query, whereclause, false) == 0) {
		if(type==MYSQLUPDATE_TYPE_SINGLE) mysql_query(dbCon, query);
		else mysql_tquery(dbCon, query);
	}
	else
	{
		new table_name[64];
		GetGVarString("MYSQL_UpdateTableName", table_name, 64);
		format(whereclause, sizeof(whereclause), " WHERE `%s`=%d", column_name, sqlid);
		strcat(query, whereclause, MAX_STRING);

		if(type==MYSQLUPDATE_TYPE_SINGLE) mysql_query(dbCon, query);
		else mysql_tquery(dbCon, query);

		format(query, MAX_STRING, "UPDATE `%s` SET ", table_name);
	}
	return 1;
}

MySQLUpdateInt(query[], sqlplayerid, sqlvalname[], sqlupdateint) // by Aktah
{
	MySQLUpdateBuild(query, sqlplayerid);
	new updval[128];
	format(updval, sizeof(updval), "`%s`=%d", sqlvalname, sqlupdateint);
	strcat(query, updval, MAX_STRING);
	return 1;
}

MySQLUpdateFlo(query[], sqlplayerid, sqlvalname[], Float:sqlupdateflo) // by Aktah
{
/*	new flotostr[32];
	format(flotostr, sizeof(flotostr), "%f", sqlupdateflo);
	MySQLUpdateStr(query, sqlplayerid, sqlvalname, flotostr);*/
	MySQLUpdateBuild(query, sqlplayerid);
	new updval[128];
	format(updval, sizeof(updval), "`%s`=%f", sqlvalname, sqlupdateflo);
	strcat(query, updval, MAX_STRING);
	return 1;
}

MySQLUpdateStr(query[], sqlplayerid, sqlvalname[], sqlupdatestr[]) // by Aktah
{
	MySQLUpdateBuild(query, sqlplayerid);
	new escstr[128];
	new updval[128];
	mysql_escape_string(sqlupdatestr, escstr);
	format(updval, sizeof(updval), "`%s`='%s'", sqlvalname, escstr);
	strcat(query, updval, MAX_STRING);
	return 1;
}